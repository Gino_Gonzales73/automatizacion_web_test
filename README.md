>Este es un proyecto para pruebas web, ahora esta solo configurado para prueba en el navegador chrome,
>en caso se quisiera agregar a los demas navegadores se tiene que descargar sus respectivos 
>driver's de su sistema operativo.
>

**CONFIGURACION**

  >1.   Se debe descargar y remplazar el driver de su navegador en la ruta del proyecto
  >     
  >     Mi local tiene la version de Chrome: 78.0.3904.87
   
  >     Documentacion: https://serenity-bdd.github.io/theserenitybook/latest/web-testing-in-serenity.html   
        
       -- version de chrome para descargar tu driver correspondiente -> chrome://settings/help
       
       -- remplazar en la siguiente ruta 
       -- resources/webdriver/windows/chromedriver.exe
       
**EJECUTAR**

    -- Para ejecutar de manera directa desde el Terminal
    
    -- gradle clean test -Pwebdriver.driver=chrome -Denvironments=default
