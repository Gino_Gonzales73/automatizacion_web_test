package com.baufest.page;

import org.openqa.selenium.By;


public class PageUserCredentials {

    // ELEMENTOS PARA EL REGISTRO
    public static final By BUTTON_SIGN_UP = By.id("signin2");
    public static final By USERNAME_FIELD = By.id("sign-username");
    public static final By PASSWORD_FIELD = By.id("sign-password");
    public static final By BUTTON_REGISTER_USER = By.xpath("btn btn-primary");//$x("//button[contains(@onclick,'register()')]")

    //ELEMENTOS PARA EL LOGIN
    public static final By BUTTON_LOGIN = By.id("login2");
    public static final By USERNAME_LOGIN_FIELD = By.id("loginusername");
    public static final By PASSWORD_LOGIN_FIELD = By.id("loginpassword");
    public static final By BUTTON_START_SESSION = By.xpath("//button[contains(@onclick,'logIn()')]");

}
