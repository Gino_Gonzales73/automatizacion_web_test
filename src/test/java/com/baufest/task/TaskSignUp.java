package com.baufest.task;


import com.baufest.page.PageUserCredentials;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.questions.WebElementQuestion;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.Wait;

public class TaskSignUp implements Task {

    private String username;
    private String password;

    public TaskSignUp(String username, String password){
        this.username = username;
        this.password = password;
    }

    public static Performable credentials(String username, String password) {
        return  Tasks.instrumented(TaskSignUp.class,username,password);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(PageUserCredentials.BUTTON_LOGIN),
                Clear.field(PageUserCredentials.USERNAME_LOGIN_FIELD),
                Enter.theValue(username).into(PageUserCredentials.USERNAME_LOGIN_FIELD),
                Clear.field(PageUserCredentials.PASSWORD_LOGIN_FIELD),
                Enter.theValue(password).into(PageUserCredentials.PASSWORD_LOGIN_FIELD)
        );
    }

}
