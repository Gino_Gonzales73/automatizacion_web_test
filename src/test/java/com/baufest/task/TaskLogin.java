package com.baufest.task;

import com.baufest.page.PageUserCredentials;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.*;
import org.openqa.selenium.Keys;

public class TaskLogin implements Task {

    private String username;
    private String password;

    public TaskLogin(String username, String password){
        this.username = username;
        this.password = password;
    }

    public static Performable credentials(String username, String password) {
        return  Tasks.instrumented(TaskSignUp.class,username,password);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(PageUserCredentials.BUTTON_SIGN_UP),
                Clear.field(PageUserCredentials.USERNAME_FIELD),
                Enter.theValue(username).into(PageUserCredentials.USERNAME_FIELD),
                Clear.field(PageUserCredentials.PASSWORD_FIELD),
                Enter.theValue(password).into(PageUserCredentials.PASSWORD_FIELD),
                Click.on(PageUserCredentials.BUTTON_START_SESSION)
        );
    }
}
