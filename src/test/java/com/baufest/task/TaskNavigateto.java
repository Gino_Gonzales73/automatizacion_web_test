package com.baufest.task;

import com.baufest.page.PageApplicationProductStore;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.demoblaze.com/index.html")
public class TaskNavigateto {
    public static Performable theApplicationProductStore(){
        return Task.where("Ingresar a la aplicacion Store page", Open.browserOn().the(PageApplicationProductStore.class));
    }
}
