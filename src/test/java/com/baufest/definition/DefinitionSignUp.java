package com.baufest.definition;

import com.baufest.task.TaskLogin;
import com.baufest.task.TaskNavigateto;
import com.baufest.task.TaskSignUp;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;


import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;


public class DefinitionSignUp {

    @Before
    public void setTheStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("que soy {string} y quiero registrarme")
    public void userio_nuevo_registrar(String name){
        theActorCalled(name).attemptsTo(TaskNavigateto.theApplicationProductStore());
    }

    @When("ingreso mi {string} y {string}")
    public void ingreso_mis_credenciales_registro(String username, String password){
        theActorInTheSpotlight().attemptsTo(TaskSignUp.credentials(username,password));
    }

    @And("me logueo con mis credenciales {string} y {string}")
    public void ingreso_mis_credenciales_login(String username, String password){
        theActorInTheSpotlight().attemptsTo(TaskLogin.credentials(username,password));
    }

    @Then("se mostrara un mensaje de registro exitoso")
    public void registro_exitoso(){

    }
}
